package com.ss.sniffer.util;

import java.io.File;
import java.net.URI;
import java.net.URL;

/**
 * @author Ronn
 */
public class Utils {

	public static File getRootFolderFromClass(final Class<?> cs) {

		String className = cs.getName();

		final StringBuilder builder = new StringBuilder(className.length());
		builder.append('/');

		for(int i = 0, length = className.length(); i < length; i++) {

			char ch = className.charAt(i);

			if(ch == '.') {
				ch = '/';
			}

			builder.append(ch);
		}

		builder.append(".class");

		className = builder.toString();

		try {

			final URL url = Utils.class.getResource(className);

			String path = url.getPath();
			path = path.substring(0, path.length() - className.length());
			path = path.substring(0, path.lastIndexOf('/'));

			final URI uri = new URI(path);
			path = uri.getPath();
			path = path.replaceAll("%20", " ");

			File file = new File(path);

			while(path.lastIndexOf('/') != -1 && !file.exists()) {
				path = path.substring(0, path.lastIndexOf('/'));
				file = new File(path);
			}

			return file;
		} catch(final Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
