package com.ss.sniffer.model;

import com.ss.sniffer.ui.model.listeners.MessageListener;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.ui.state.UIState;

/**
 * Интерфейс для реализации работника с пакетами.
 * 
 * @author Ronn
 */
public interface Worker {

	/**
	 * Подписаться на все реализованные слушатели.
	 * 
	 * @param object подписываемый объект.
	 */
	public void addListener(Object object);

	/**
	 * @param listener добавляемый слушатель сообщений.
	 */
	public void addMessageListener(MessageListener listener);

	/**
	 * @return получение текущей выбранной сессии.
	 */
	public Session getCurrentSession();

	/**
	 * Получение текущей UI стадии.
	 * 
	 * @return текущая UI стадия.
	 */
	public UIState getUIStage();

	/**
	 * Отписаться от всех реализованных слушателей.
	 * 
	 * @param object отписываемый объект.
	 */
	public void removeListener(Object object);

	/**
	 * @param listener удаляемый слушатель сообщений.
	 */
	public void removeMessageListener(MessageListener listener);

	/**
	 * Удаление сессии из работника.
	 * 
	 * @param session удаляемая сессия.
	 */
	public void removeSession(Session session);

	/**
	 * Отправка сообщения работнику.
	 * 
	 * @param message отпровляемое сообщение.
	 */
	public void sendMessage(String message);

	/**
	 * Отправка выснифаного пакета на дальнейшую обработку.
	 * 
	 * @param packet выснифанный пакет.
	 */
	public void sendPacket(Packet packet);

	/**
	 * Установка текуще выбранной пакетной сессии.
	 * 
	 * @param session пакетная сессия.
	 */
	public void setCurrentSession(Session session);
}
