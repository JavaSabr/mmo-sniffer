package com.ss.sniffer.model.impl;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.session.event.SessionEvent;
import com.ss.sniffer.model.session.event.impl.AddPacketEvent;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.listeners.SessionListener;
import com.ss.sniffer.ui.model.packet.UIPacket;
import com.ss.sniffer.ui.uistage.main.MainSessionTab;
import com.ss.sniffer.model.session.event.impl.ReloadPacketsEvent;
import com.ss.sniffer.ui.model.packet.protocol.PacketDescription;
import com.ss.sniffer.ui.model.packet.protocol.Protocol;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Базовая реализация пакетной сессии.
 * 
 * @author Ronn
 */
public class DefaultSession implements Session {

	/** слушатели сессии */
	private final Array<SessionListener> listeners;

	/** хранилище полученных пакетов */
	private final Array<UIPacket> currentPackets;

	/** ид сессии */
	private final int id;

	/** текущий протокол сессии */
	private Protocol protocol;

	public DefaultSession(final int id, final MainSessionTab mainTab) {
		this.id = id;
		this.listeners = ArrayFactory.newArray(SessionListener.class);
		this.currentPackets = ArrayFactory.newArray(UIPacket.class);
	}

	@Override
	public void addListener(final SessionListener object) {
		listeners.add(object);
	}

	@Override
	public void addPacket(final UIPacket uiPacket) {

		currentPackets.add(uiPacket);

		uiPacket.setName(UIPacket.UNKNOWN_NAME);

		final Protocol protocol = getProtocol();

		if(protocol != null) {

			final PacketDescription description = protocol.getDescription(uiPacket.getOpcode(), uiPacket.getType() == PacketType.SERVER_PACKET);

			if(description != null) {
				uiPacket.setName(description.getName());
			} else {
				uiPacket.setName(UIPacket.UNKNOWN_NAME);
			}
		}

		notifyListeners(new AddPacketEvent(null, uiPacket));
	}

	@Override
	public void clear() {
		currentPackets.clear();
	}

	private Array<UIPacket> getCurrentPackets() {
		return currentPackets;
	}

	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return слушатели сессии.
	 */
	private Array<SessionListener> getListeners() {
		return listeners;
	}

	@Override
	public void getPackets(final Array<Packet> container) {

		final Array<UIPacket> packets = getCurrentPackets();

		for(final UIPacket uiPacket : packets) {
			container.add(uiPacket.getPacket());
		}
	}

	@Override
	public Protocol getProtocol() {
		return protocol;
	}

	private void notifyListeners(final SessionEvent event) {

		final Array<SessionListener> listeners = getListeners();

		for(final SessionListener listener : listeners) {
			listener.notify(this, event);
		}
	}

	@Override
	public synchronized void reload() {

		final Array<UIPacket> packets = getCurrentPackets();

		notifyListeners(new ReloadPacketsEvent());

		final Protocol protocol = getProtocol();

		if(protocol != null) {
			for(final UIPacket packet : packets) {

				final PacketDescription description = protocol.getDescription(packet.getOpcode(), packet.getType() == PacketType.SERVER_PACKET);

				if(description != null) {
					packet.setName(description.getName());
				} else {
					packet.setName(UIPacket.UNKNOWN_NAME);
				}
			}
		}

		for(final UIPacket packet : packets) {
			notifyListeners(new AddPacketEvent(null, packet));
		}
	}

	@Override
	public void removeListener(final SessionListener object) {
		listeners.fastRemove(object);
	}

	@Override
	public void setProcotol(final Protocol protocol) {
		this.protocol = protocol;
	}
}
