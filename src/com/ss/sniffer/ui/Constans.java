package com.ss.sniffer.ui;

import javafx.geometry.Insets;

/**
 * Класс с набором констант.
 * 
 * @author Ronn
 */
public final class Constans {

	public static final Insets INSETS_1 = new Insets(1);
	public static final Insets INSETS_2 = new Insets(2);
	public static final Insets INSETS_3 = new Insets(3);
	public static final Insets INSETS_4 = new Insets(4);
	public static final Insets INSETS_5 = new Insets(5);

}
