package com.ss.sniffer.ui.control.packet.table;

import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.UIUtils;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import rlib.util.StringUtils;

/**
 * Реализация ячейки таблици с отображением иконки типа пакета.
 * 
 * @author Ronn
 */
public class PacketTypeTableCell extends TableCell<PacketItem, PacketType> {

	private static final String SERVER_PACKET_IMAGE_NAME = "server_packet_16";
	private static final String CLIENT_PACKET_IMAGE_NAME = "client_packet_16";

	public static final Callback<TableColumn<PacketItem, PacketType>, TableCell<PacketItem, PacketType>> FACTORY = param -> new PacketTypeTableCell();

	private static Image clientPacket;
	private static Image serverPacket;

	public static Image getClientPacket() {

		if(clientPacket == null) {
			clientPacket = UIUtils.loadImage(CLIENT_PACKET_IMAGE_NAME);
		}

		return clientPacket;
	}

	public static Image getServerPacket() {

		if(serverPacket == null) {
			serverPacket = UIUtils.loadImage(SERVER_PACKET_IMAGE_NAME);
		}

		return serverPacket;
	}

	@Override
	protected void updateItem(final PacketType item, final boolean empty) {

		if(item == null) {
			setText(StringUtils.EMPTY);
			return;
		}

		final boolean isClient = item == PacketType.CLIENT_PACKET;

		final Image image = isClient ? getClientPacket() : getServerPacket();

		setGraphic(new ImageView(image));
		setAlignment(Pos.CENTER);
	}
}
