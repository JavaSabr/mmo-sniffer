package com.ss.sniffer.ui.control.packet.table;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.PacketModel;

import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import rlib.util.StringUtils;

/**
 * Реализация ячейки таблици с отображением опкода пакета.
 * 
 * @author Ronn
 */
public class PacketOpcodeTableCell extends TableCell<PacketItem, Integer> {

	public static final Callback<TableColumn<PacketItem, Integer>, TableCell<PacketItem, Integer>> FACTORY = param -> new PacketOpcodeTableCell();

	@Override
	protected void updateItem(final Integer item, final boolean empty) {

		if(item == null) {
			setText(StringUtils.EMPTY);
			return;
		}

		final PacketModel packetModel = Config.PACKET_MODEL;

		final int length = packetModel.getOpcodeLength() * 2;

		final StringBuilder builder = new StringBuilder(length);
		builder.append("0x");
		builder.append(Integer.toHexString(item).toUpperCase());

		while(builder.length() < length) {
			builder.insert(0, '0');
		}

		setText(builder.toString());
		setAlignment(Pos.CENTER);
	}
}
