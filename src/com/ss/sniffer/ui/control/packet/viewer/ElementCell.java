package com.ss.sniffer.ui.control.packet.viewer;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import rlib.util.StringUtils;

/**
 * Модель UI элемента дерева структура пакета.
 * 
 * @author Ronn
 */
public class ElementCell extends TreeCell<DescriptionElement> {

	public static final Callback<TreeView<DescriptionElement>, TreeCell<DescriptionElement>> FACTORY = param -> new ElementCell();

	@Override
	public void updateItem(final DescriptionElement item, final boolean empty) {

		super.updateItem(item, empty);

		if(item == null) {
			setText(StringUtils.EMPTY);
			setGraphic(null);
			setTooltip(null);
		} else {

			final Object value = item.getValue();

			if(value != null && value != StringUtils.EMPTY) {
				setText(item.getName() + " - " + String.valueOf(item.getValue()));
			} else {
				setText(item.getName());
			}

			final String byteValue = item.getByteValue();

			if(!StringUtils.isEmpty(byteValue)) {
				setTooltip(new Tooltip(item.getByteValue()));
			}

			final Image image = item.getImage();

			setGraphic(image == null ? null : new ImageView(image));
		}
	}
}
