package com.ss.sniffer.ui.control.packet.viewer;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.model.Session;
import com.ss.sniffer.ui.Constans;
import com.ss.sniffer.ui.control.packet.table.PacketItem;
import com.ss.sniffer.ui.model.packet.UIPacket;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

import rlib.util.StringUtils;

/**
 * Комбинировый отображатель пакетов.
 * 
 * @author Ronn
 */
public class MultiPacketViewer extends VBox implements ChangeListener<PacketItem>, PacketViewer {

	private static final int NAME_FIELD_MIN_WIDTH = 170;
	private static final int NAME_LABEL_MIN_WIDTH = 90;

	private static final int OPCODE_FIELD_WIDTH = 80;
	private static final int OPCODE_LABEL_MIN_WIDTH = 70;

	private static final String NAME_LABEL_NAME = "Название:";
	private static final String OPCODE_LABEL_NAME = "Опкод:";

	/** байтовый просмоторщик пакета */
	private final PacketByteViewer byteViewer;
	private final PacketDescriptionViewer descriptionViewer;

	/** отображение текущего названия пакета */
	private final TextField opcodeField;
	private final TextField nameField;

	/** отображение текущего опкода пакета */
	private final Label opcodeLabel;
	private final Label nameLabel;

	/** переключатель режимом отображения */
	private final CheckBox viewerSwitcher;

	/** текущая пакетная сессия */
	private Session session;

	public MultiPacketViewer(final Region root) {

		byteViewer = new PacketByteViewer();
		descriptionViewer = new PacketDescriptionViewer();

		final ObservableList<Node> children = getChildren();

		opcodeField = new TextField();
		opcodeField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		opcodeField.setMinWidth(OPCODE_FIELD_WIDTH);
		opcodeField.setMaxWidth(OPCODE_FIELD_WIDTH);
		opcodeField.setEditable(false);
		opcodeField.prefWidthProperty().bind(root.widthProperty());

		nameField = new TextField();
		nameField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		nameField.setMinWidth(NAME_FIELD_MIN_WIDTH);
		nameField.setEditable(false);
		nameField.prefWidthProperty().bind(root.widthProperty());

		opcodeLabel = new Label(OPCODE_LABEL_NAME);
		opcodeLabel.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		opcodeLabel.setAlignment(Pos.CENTER_RIGHT);
		opcodeLabel.setMinWidth(OPCODE_LABEL_MIN_WIDTH);

		nameLabel = new Label(NAME_LABEL_NAME);
		nameLabel.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		nameLabel.setAlignment(Pos.CENTER_RIGHT);
		nameLabel.setMinWidth(NAME_LABEL_MIN_WIDTH);

		viewerSwitcher = new CheckBox("Структурный");
		viewerSwitcher.setAlignment(Pos.CENTER);
		viewerSwitcher.setMinWidth(140);
		viewerSwitcher.addEventHandler(ActionEvent.ACTION, event -> switchViewer());

		final HBox hBox = new HBox(4);
		hBox.setAlignment(Pos.CENTER);
		hBox.getChildren().addAll(opcodeLabel, opcodeField, nameLabel, nameField, viewerSwitcher);

		VBox.setMargin(hBox, new Insets(10, 5, 0, 5));
		VBox.setMargin(byteViewer, Constans.INSETS_5);
		VBox.setMargin(descriptionViewer, new Insets(10));

		children.add(hBox);
		children.add(byteViewer);

		byteViewer.prefHeightProperty().bind(root.heightProperty());
		descriptionViewer.prefHeightProperty().bind(root.heightProperty());
	}

	@Override
	public void changed(final ObservableValue<? extends PacketItem> observable, final PacketItem oldValue, final PacketItem newValue) {
		setPacket(getSession(), newValue == null ? null : newValue.getPacket());
	}

	/**
	 * @return байтовый отображатель пакета.
	 */
	private PacketByteViewer getByteViewer() {
		return byteViewer;
	}

	/**
	 * @return структурный просмоторщик пакета.
	 */
	private PacketDescriptionViewer getDescriptionViewer() {
		return descriptionViewer;
	}

	/**
	 * @return текущая пакетная сессия.
	 */
	private final Session getSession() {
		return session;
	}

	/**
	 * @return переключатель режимов отображения пакетов.
	 */
	private CheckBox getViewerSwitcher() {
		return viewerSwitcher;
	}

	/**
	 * @return отображать ли в структурном виде пакет.
	 */
	private boolean isStructuredView() {
		return getViewerSwitcher().isSelected();
	}

	@Override
	public void setPacket(final Session session, final UIPacket uiPacket) {

		final PacketViewer packetViewer = isStructuredView() ? descriptionViewer : getByteViewer();

		if(packetViewer != null) {
			packetViewer.setPacket(session, uiPacket);
		}

		if(uiPacket == null) {
			opcodeField.setText(StringUtils.EMPTY);
			nameField.setText(StringUtils.EMPTY);
		} else {

			final PacketModel packetModel = Config.PACKET_MODEL;

			final int length = packetModel.getOpcodeLength() * 2;

			final StringBuilder builder = new StringBuilder(length);
			builder.append("0x");
			builder.append(Integer.toHexString(uiPacket.getOpcode()).toUpperCase());

			while(builder.length() < length) {
				builder.insert(0, '0');
			}

			opcodeField.setText(builder.toString());
			nameField.setText(uiPacket.getName());
		}
	}

	public void setSession(final Session session) {
		this.session = session;
	}

	/**
	 * Переключение между различными видами просмотра пакета.
	 */
	private void switchViewer() {

		final CheckBox switcher = getViewerSwitcher();

		final PacketByteViewer byteViewer = getByteViewer();
		final PacketDescriptionViewer descriptionViewer = getDescriptionViewer();

		final boolean selected = switcher.isSelected();

		final ObservableList<Node> children = getChildren();

		if(selected && !children.contains(descriptionViewer)) {
			children.remove(byteViewer);
			children.add(descriptionViewer);
		} else if(!selected && !children.contains(byteViewer)) {
			children.add(byteViewer);
			children.remove(descriptionViewer);
		}
	}
}
