package com.ss.sniffer.ui.control.packet.viewer;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.ui.model.packet.UIPacket;

/**
 * Интерфейс для реализации просмоторщика пакета.
 * 
 * @author Ronn
 */
public interface PacketViewer {

	/**
	 * Установка просматриваемого пакета.
	 * 
	 * @param session текущая пакетная сессия.
	 * @param uiPacket просматриваемый пакет.
	 */
	public void setPacket(Session session, UIPacket uiPacket);
}
