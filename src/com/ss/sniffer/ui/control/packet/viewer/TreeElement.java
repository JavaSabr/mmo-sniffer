package com.ss.sniffer.ui.control.packet.viewer;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * Реализация элемента дерева структуры пакета.
 * 
 * @author Ronn
 */
public class TreeElement extends TreeItem<DescriptionElement> {

	public TreeElement(final DescriptionElement element) {

		setValue(element);
		setExpanded(true);

		final Image image = element.getImage();

		if(image != null) {
			setGraphic(new ImageView(image));
		}

		final DescriptionElement[] elements = element.getChildren();

		if(elements != null && elements.length > 0) {

			final ObservableList<TreeItem<DescriptionElement>> children = getChildren();

			for(final DescriptionElement child : elements) {
				children.add(new TreeElement(child));
			}
		}
	}
}
