package com.ss.sniffer.ui.model;

/**
 * Перечисление типов менюшек в панели меню.
 * 
 * @author Ronn
 */
public enum MenuCategory {

	MAIN("Главное"),
	SESSION("Сессия"),
	PARSER("Парсеры"), ;

	/** название менюхи */
	private String name;

	private MenuCategory(final String name) {
		this.name = name;
	}

	/**
	 * @return название менюшки.
	 */
	public String getName() {
		return name;
	}
}
