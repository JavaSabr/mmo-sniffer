package com.ss.sniffer.ui.model;

import com.ss.sniffer.model.Worker;
import com.ss.sniffer.ui.state.UIState;

import javafx.scene.control.Tab;

/**
 * Интерфейс для реализации рабочей вкладки.
 * 
 * @author Ronn
 */
public interface WorkerTab {

	/**
	 * Завершение работы вкладки.
	 */
	public void finish();

	/**
	 * @return таб для размещение в основных вкладках.
	 */
	public Tab getTab();

	/**
	 * Подготовка вкладки для работы.
	 * 
	 * @param uiStage UI стадия.
	 * @param worker работник снифера.
	 */
	public void prepare(UIState uiStage, Worker worker);
}
