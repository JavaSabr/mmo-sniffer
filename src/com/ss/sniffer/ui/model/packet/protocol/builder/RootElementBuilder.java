package com.ss.sniffer.ui.model.packet.protocol.builder;

import java.nio.ByteBuffer;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

import rlib.util.array.Array;

/**
 * Рутовый строитель элементов пакета.
 * 
 * @author Ronn
 */
public final class RootElementBuilder extends AbstractElementBuilder {

	private static final String NAME = "root";

	@Override
	public void createElements(final ByteBuffer buffer, final Array<DescriptionElement> container) {
		for(final ElementBuilder builder : children) {
			builder.createElements(buffer, container);
		}
	}

	@Override
	public String getName() {
		return NAME;
	}
}
