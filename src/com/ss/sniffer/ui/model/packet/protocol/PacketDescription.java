package com.ss.sniffer.ui.model.packet.protocol;

import com.ss.sniffer.net.packet.PacketType;
import com.ss.sniffer.ui.model.packet.UIPacket;

/**
 * Интерфейс для описания пакета.
 * 
 * @author Ronn
 */
public interface PacketDescription {

	/**
	 * @param uiPacket пакет, для которого надо получить структуру.
	 * @return список элементов описывающих пакет.
	 */
	public DescriptionElement[] createElements(UIPacket uiPacket);

	/**
	 * @return название пакета.
	 */
	public String getName();

	/**
	 * @return опкод пакета.
	 */
	public int getOpcode();

	/**
	 * @return тип пакета.
	 */
	public PacketType getType();
}
