package com.ss.sniffer.ui.model.packet.protocol.builder;

import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Базовая реализация билдера элементов пакета.
 * 
 * @author Ronn
 */
public abstract class AbstractElementBuilder implements ElementBuilder {

	/** список внутренних элементов */
	protected final Array<ElementBuilder> children;

	/** название элемента */
	private String elementName;

	public AbstractElementBuilder() {
		this.children = ArrayFactory.newArray(ElementBuilder.class);
	}

	@Override
	public void addChildren(final ElementBuilder builder) {
		children.add(builder);
	}

	@Override
	public String getElementName() {
		return elementName;
	}

	@Override
	public void setElementName(final String elementName) {
		this.elementName = elementName;
	}
}
