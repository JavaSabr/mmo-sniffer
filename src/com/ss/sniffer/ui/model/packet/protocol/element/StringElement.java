package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;

import javafx.scene.image.Image;

/**
 * Реализация элемента представляющего строку.
 * 
 * @author Ronn
 */
public class StringElement extends AbstractDescriptionElement<String> {

	private static final String IMAGE_NAME = "string_field_16";

	public StringElement(final String name, final String byteValue, final String value) {
		super(name, byteValue, value);
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(IMAGE_NAME);
	}
}
