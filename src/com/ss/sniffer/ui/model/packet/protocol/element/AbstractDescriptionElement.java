package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

/**
 * Базовая реализация элемента описания структуры пакета.
 * 
 * @author Ronn
 */
public abstract class AbstractDescriptionElement<V> implements DescriptionElement {

	/** название элемента */
	private final String name;
	/** байтовое значение элемента */
	private final String byteValue;

	/** итоговое значение элемента */
	protected final V value;

	public AbstractDescriptionElement(final String name, final String byteValue, final V value) {
		this.name = name;
		this.byteValue = byteValue;
		this.value = value;
	}

	@Override
	public String getByteValue() {
		return byteValue;
	}

	@Override
	public DescriptionElement[] getChildren() {
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getValue() {
		return (T) value;
	}
}
