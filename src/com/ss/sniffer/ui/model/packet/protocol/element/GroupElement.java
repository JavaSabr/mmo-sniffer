package com.ss.sniffer.ui.model.packet.protocol.element;

import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.model.packet.protocol.DescriptionElement;

import javafx.scene.image.Image;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Реалиация элемента группы.
 * 
 * @author Ronn
 */
public class GroupElement extends AbstractDescriptionElement<Object> {

	private static final String GROUP_IMAGE_NAME = "group_16";

	/** список дочерних элементов */
	private final Array<DescriptionElement> children;

	public GroupElement(final String name, final String byteValue, final Object value) {
		super(name, byteValue, value);

		this.children = ArrayFactory.newArray(DescriptionElement.class);
	}

	/**
	 * Добавление нового дочернего элемента.
	 * 
	 * @param element новый дочерний элемент.
	 */
	public void addChildren(final DescriptionElement element) {
		children.add(element);
	}

	@Override
	public DescriptionElement[] getChildren() {
		children.trimToSize();
		return children.array();
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(GROUP_IMAGE_NAME);
	}
}
