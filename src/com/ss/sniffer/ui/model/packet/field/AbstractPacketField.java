package com.ss.sniffer.ui.model.packet.field;

import javafx.scene.image.Image;

/**
 * Базовая реализация поля пакета.
 * 
 * @author Ronn
 */
public abstract class AbstractPacketField<T> implements PacketField {

	/** значение поля */
	protected T value;

	/** название поля */
	protected String name;

	@Override
	public Image getImage() {
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <U> U getValue() {
		return (U) value;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}
}
