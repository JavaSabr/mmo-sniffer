package com.ss.sniffer.ui.model.packet.field;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class LongField extends AbstractPacketField<Number> {

	@Override
	public Class<?> getType() {
		return Long.class;
	}

	@Override
	public void read(final ByteBuffer buffer) {
		value = Long.valueOf(buffer.getLong());
	}
}
