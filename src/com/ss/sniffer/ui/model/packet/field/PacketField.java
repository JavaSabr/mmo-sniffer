package com.ss.sniffer.ui.model.packet.field;

import java.nio.ByteBuffer;

import javafx.scene.image.Image;

/**
 * Интерфейс для реализации поля пакета.
 * 
 * @author Ronn
 */
public interface PacketField {

	/**
	 * @return изображения.
	 */
	public Image getImage();

	/**
	 * @return название поля.
	 */
	public String getName();

	/**
	 * @return тип поля.
	 */
	public Class<?> getType();

	/**
	 * @return значение поля.
	 */
	public <T> T getValue();

	/**
	 * Чтение значения поля из данных пакета.
	 * 
	 * @param buffer буффер с данными.
	 */
	public void read(ByteBuffer buffer);

	/**
	 * @param name название поля.
	 */
	public void setName(String name);
}
