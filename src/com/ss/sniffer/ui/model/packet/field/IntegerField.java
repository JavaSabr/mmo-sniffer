package com.ss.sniffer.ui.model.packet.field;

import java.nio.ByteBuffer;

/**
 * @author Ronn
 */
public class IntegerField extends AbstractPacketField<Number> {

	@Override
	public Class<?> getType() {
		return Integer.class;
	}

	@Override
	public void read(final ByteBuffer buffer) {
		value = Long.valueOf(buffer.getInt() & 0xFFFFFFFF);
	}
}
