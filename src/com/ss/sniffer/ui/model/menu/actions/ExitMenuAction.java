package com.ss.sniffer.ui.model.menu.actions;

import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.model.MenuAction;
import com.ss.sniffer.ui.model.MenuCategory;
import com.ss.sniffer.model.Worker;

import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;

/**
 * Действия для выхода из программы.
 * 
 * @author Ronn
 */
public class ExitMenuAction implements MenuAction {

	private static final String CLOSE_IMAGE_NAME = "close_16";
	private static final String ACTION_NAME = "Выход";

	@Override
	public void execute(final MenuItem item, final Worker worker) {
	}

	@Override
	public void execute(final Worker worker) {
		System.exit(0);
	}

	@Override
	public MenuCategory getCategory() {
		return MenuCategory.MAIN;
	}

	@Override
	public MenuItem[] getChildren() {
		return null;
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(CLOSE_IMAGE_NAME);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}
}
