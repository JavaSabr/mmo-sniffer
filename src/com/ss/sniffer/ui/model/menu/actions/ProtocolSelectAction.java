package com.ss.sniffer.ui.model.menu.actions;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.model.MenuAction;
import com.ss.sniffer.ui.model.MenuCategory;
import com.ss.sniffer.manager.ProtocolManager;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.ui.model.packet.protocol.Protocol;
import com.ss.sniffer.ui.state.UIState;

import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import rlib.util.array.Array;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

/**
 * Меню с выбором протокола текущей сессии.
 * 
 * @author Ronn
 */
public class ProtocolSelectAction implements MenuAction {

	private static final String IMAGE_NAME = "protocol_16";
	private static final String ACTION_NAME = "Протокол";

	/** таблица протоколов доступных для акшена */
	private final Table<MenuItem, Protocol> itemTable;

	public ProtocolSelectAction() {
		this.itemTable = TableFactory.newObjectTable();
	}

	@Override
	public void execute(final MenuItem item, final Worker worker) {

		final Session session = worker.getCurrentSession();

		final UIState uiStage = worker.getUIStage();

		if(session == null) {
			UIUtils.showMessage(uiStage, "Отсутствует выбранная сессия", false);
			return;
		}

		session.setProcotol(itemTable.get(item));
		session.reload();
	}

	@Override
	public void execute(final Worker worker) {
	}

	@Override
	public MenuCategory getCategory() {
		return MenuCategory.SESSION;
	}

	@Override
	public MenuItem[] getChildren() {

		final ProtocolManager manager = ProtocolManager.getInstance();

		final Array<Protocol> protocols = manager.getAvailableProtocols();

		final MenuItem[] items = new MenuItem[protocols.size()];

		for(int i = 0, length = items.length; i < length; i++) {

			final Protocol protocol = protocols.get(i);

			final MenuItem item = new MenuItem(protocol.getName() + " " + protocol.getVersion());

			itemTable.put(item, protocol);

			items[i] = item;
		}

		return items;
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(IMAGE_NAME);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}
}
