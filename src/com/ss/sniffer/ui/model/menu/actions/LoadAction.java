package com.ss.sniffer.ui.model.menu.actions;

import java.io.File;
import java.io.IOException;

import com.ss.sniffer.Config;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.model.MenuAction;
import com.ss.sniffer.ui.model.MenuCategory;
import com.ss.sniffer.ui.state.UIState;
import com.ss.sniffer.util.Utils;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

/**
 * Реализация акшена для сохранения пакетов сессии.
 * 
 * @author Ronn
 */
public class LoadAction implements MenuAction {

	private static final String OPEN_IMAGE_NAME = "open_16";
	private static final String TITLE = "Загрузка сессии";
	private static final String ACTION_NAME = "Загрузить сессию";

	/** окно выбора файла */
	private final FileChooser fileChooser;

	/** стадия УИ для выбора файла */
	private final Stage stage;

	public LoadAction() throws IOException {

		fileChooser = new FileChooser();
		fileChooser.setTitle(TITLE);
		fileChooser.setInitialDirectory(Utils.getRootFolderFromClass(Config.ROOT_CLASS).getCanonicalFile());

		final ObservableList<ExtensionFilter> filters = fileChooser.getExtensionFilters();
		filters.add(new ExtensionFilter(NetUtil.SESSION_DUMP_FORMAT, "*" + NetUtil.SESSION_DUMP_FORMAT));

		stage = new Stage();
		stage.initModality(Modality.WINDOW_MODAL);
	}

	@Override
	public void execute(final MenuItem item, final Worker worker) {
	}

	@Override
	public synchronized void execute(final Worker worker) {

		final UIState uiStage = worker.getUIStage();

		final Array<Packet> packets = ArrayFactory.newArray(Packet.class);

		stage.initOwner(uiStage.getStage());

		final File file = fileChooser.showOpenDialog(stage);

		if(file == null) {
			return;
		}

		if(!file.canRead()) {
			UIUtils.showMessage(uiStage, "Этот фаил не доступен для чтения.", false);
			return;
		}

		final Runnable readTask = () -> {

			NetUtil.load(file, packets);

			final Runnable loadTask = () -> {
				for(final Packet packet : packets) {
					worker.sendPacket(packet);
				}
			};

			Platform.runLater(loadTask);
		};

		UIUtils.showInfinityProgress(uiStage, readTask);
	}

	@Override
	public MenuCategory getCategory() {
		return MenuCategory.SESSION;
	}

	@Override
	public MenuItem[] getChildren() {
		return null;
	}

	@Override
	public Image getImage() {
		return UIUtils.loadImage(OPEN_IMAGE_NAME);
	}

	@Override
	public String getName() {
		return ACTION_NAME;
	}
}
