package com.ss.sniffer.ui.model.session.tabs;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Обработчик переключения типов таблиц пакетов.
 * 
 * @author Ronn
 */
public class TableChangeHandler implements EventHandler<ActionEvent> {

	private final PacketView packetViewer;

	public TableChangeHandler(final PacketView packetViewer) {
		this.packetViewer = packetViewer;
	}

	@Override
	public void handle(final ActionEvent event) {
		packetViewer.switchTable();
	}
}
