package com.ss.sniffer.ui.model.listeners;

/**
 * Интерфейс для реализации слушателя сообщений.
 * 
 * @author Ronn
 */
public interface MessageListener {

	/**
	 * @param message новое сообщение.
	 */
	public void addMessage(String message);
}
