package com.ss.sniffer.ui.uistage.config;

import com.ss.sniffer.Config;
import com.ss.sniffer.ui.state.impl.ConfigUIState;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SingleSelectionModel;

/**
 * Хендлер для внесения в конфиг выбранный тип криптора.
 * 
 * @author Ronn
 */
public class ChangeCryptTypeHandler implements EventHandler<ActionEvent> {

	private final ConfigUIState configStage;

	public ChangeCryptTypeHandler(final ConfigUIState configStage) {
		this.configStage = configStage;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void handle(final ActionEvent event) {

		final Object source = event.getSource();

		if(source instanceof ComboBox) {

			final ComboBox<CryptTypeItem> choiceBox = (ComboBox<CryptTypeItem>) source;

			final SingleSelectionModel<CryptTypeItem> selection = choiceBox.getSelectionModel();

			if(selection.isEmpty()) {
				Config.CRYPT_TYPE = null;
				return;
			}

			final CryptTypeItem item = selection.getSelectedItem();

			Config.CRYPT_TYPE = item.getCrypt();
		}

		configStage.update();
	}
}
