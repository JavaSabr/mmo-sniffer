package com.ss.sniffer.ui.uistage.config;

import com.ss.sniffer.Config;
import com.ss.sniffer.ui.state.impl.ConfigUIState;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import rlib.util.StringUtils;

/**
 * Валидатор ввода порта.
 * 
 * @author Ronn
 */
public class PortValidateHandler implements EventHandler<KeyEvent> {

	private final ConfigUIState configStage;

	private final TextField textField;

	public PortValidateHandler(final ConfigUIState configStage, final TextField textField) {
		this.configStage = configStage;
		this.textField = textField;
	}

	private TextField getTextField() {
		return textField;
	}

	@Override
	public void handle(final KeyEvent event) {

		final TextField textField = getTextField();

		try {
			switch(event.getCode()) {
				case DELETE:
				case BACK_SPACE:
					textField.setText(StringUtils.EMPTY);
				default: {
					validate(textField.getText() + event.getText());
				}
			}
		} finally {
			configStage.update();
		}
	}

	private void validate(final String text) {

		int port = -1;

		try {
			if(text.isEmpty()) {
				return;
			}

			port = Integer.parseInt(text);

			if(port < 1 || port > 65000) {
				port = -1;
				throw new RuntimeException();
			}
		} finally {
			Config.NETWORK_PORT = port;
		}
	}
}
