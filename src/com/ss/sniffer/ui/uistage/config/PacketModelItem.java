package com.ss.sniffer.ui.uistage.config;

import com.ss.sniffer.model.PacketModel;

/**
 * UI итем для отображения информации пакетной модели.
 * 
 * @author Ronn
 */
public class PacketModelItem {

	/** пакетная модель */
	private final PacketModel packetModel;

	public PacketModelItem(final PacketModel packetModel) {
		this.packetModel = packetModel;
	}

	/**
	 * @return пакетная модель.
	 */
	public PacketModel getPacketModel() {
		return packetModel;
	}

	@Override
	public String toString() {
		return packetModel.getName();
	}
}
