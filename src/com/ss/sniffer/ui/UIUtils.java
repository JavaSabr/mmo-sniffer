package com.ss.sniffer.ui;

import com.ss.sniffer.Config;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import com.ss.sniffer.Start;
import com.ss.sniffer.manager.ExecutorManager;
import com.ss.sniffer.ui.state.UIState;

/**
 * Набор утильных методов по UI.
 * 
 * @author Ronn
 */
public class UIUtils {

	public static final String FX_TEXT_FILL_RED = "  -fx-text-fill: red";

	private static final int PROGRESS_DIALOG_HEIGHT = 70;
	private static final int PROGRESS_DIALOG_WIDTH = 300;

	private static final String MESSAGE_TITLE = "Сообщение";
	private static final String ERROR_TITLE = "Ошибка";

	/**
	 * Загружает изображение по его имени.
	 * 
	 * @param name имя изображения без расширения, которое находится в папке
	 * ресурсов.
	 * @return загруженное изображение.
	 */
	public static Image loadImage(final String name) {
		return new Image(Start.class.getResourceAsStream("/icons/" + name + ".png"));
	}

	public static void showException(final UIState state, final Exception exception) {

		state.disable();

		final Stage stage = new Stage();
		stage.initOwner(state.getStage());
		stage.initModality(Modality.WINDOW_MODAL);

		final Font font = Config.MESSAGE_FONT;
		final String message = exception.getMessage();

		final double heght = font.getSize() + 40;
		final double width = font.getSize() * message.length() * 0.8;

		final VBox vBox = new VBox(1);

		final Scene scene = new Scene(vBox, width, heght, Color.WHITESMOKE);

		final Label label = new Label(message);
		label.setFont(font);
		label.setStyle(FX_TEXT_FILL_RED);

		vBox.getChildren().add(label);
		vBox.setAlignment(Pos.CENTER);

		stage.setTitle(ERROR_TITLE);
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, state.getMessageDialogHandler());
		stage.setMaxWidth(width);
		stage.setMinWidth(width);
		stage.setMaxHeight(heght);
		stage.setMinHeight(heght);
		stage.setScene(scene);
		stage.show();
		stage.centerOnScreen();
	}

	/**
	 * Отобразить полоску прогресса на время исполнения задачи.
	 * 
	 * @param state UI стадия, из которой задача вызывается.
	 * @param runnable задача, которую нужно исполнить.
	 */
	public static void showInfinityProgress(final UIState state, final Runnable runnable) {

		state.disable();

		final Stage stage = new Stage();
		stage.initOwner(state.getStage());
		stage.initModality(Modality.WINDOW_MODAL);

		final VBox vBox = new VBox(1);

		final Scene scene = new Scene(vBox, PROGRESS_DIALOG_WIDTH, PROGRESS_DIALOG_HEIGHT, Color.WHITESMOKE);

		final ProgressBar progressBar = new ProgressBar(ProgressIndicator.INDETERMINATE_PROGRESS);
		progressBar.setMinWidth(PROGRESS_DIALOG_WIDTH - 40);
		progressBar.setMinHeight(PROGRESS_DIALOG_HEIGHT / 2);

		vBox.getChildren().add(progressBar);
		vBox.setAlignment(Pos.CENTER);

		VBox.setMargin(progressBar, new Insets(10));

		stage.setTitle("Прогресс...");
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, state.getMessageDialogHandler());
		stage.addEventHandler(WindowEvent.WINDOW_HIDDEN, state.getMessageDialogHandler());
		stage.setMaxWidth(PROGRESS_DIALOG_WIDTH);
		stage.setMinWidth(PROGRESS_DIALOG_WIDTH);
		stage.setMaxHeight(PROGRESS_DIALOG_HEIGHT);
		stage.setMinHeight(PROGRESS_DIALOG_HEIGHT);
		stage.setScene(scene);
		stage.show();
		stage.setResizable(false);
		stage.centerOnScreen();

		final ExecutorManager executorManager = ExecutorManager.getInstance();
		executorManager.execute(() -> {
			runnable.run();
			Platform.runLater(() -> stage.close());
		});
	}

	public static void showMessage(final UIState uiStage, final String message, final boolean error) {

		uiStage.disable();

		final Stage stage = new Stage();
		stage.initOwner(uiStage.getStage());
		stage.initModality(Modality.WINDOW_MODAL);

		final Font font = Config.MESSAGE_FONT;

		final double heght = font.getSize() + 40 + +(Config.MIN_ELEMENT_HEIGHT * 2);
		final double width = Math.max(font.getSize() * message.length() * 0.8, 300);

		final VBox vBox = new VBox(1);

		final Scene scene = new Scene(vBox, width, heght, Color.WHITESMOKE);

		final Label label = new Label(message);
		label.setFont(font);

		if(error) {
			label.setStyle(FX_TEXT_FILL_RED);
		}

		final Button button = new Button("ОК");
		button.setMaxHeight(Config.MIN_ELEMENT_HEIGHT);
		button.setMinWidth(150);
		button.addEventHandler(ActionEvent.ACTION, event -> {
			stage.close();
		});

		vBox.getChildren().addAll(label, button);
		vBox.setAlignment(Pos.CENTER);

		VBox.setMargin(label, Constans.INSETS_3);
		VBox.setMargin(button, Constans.INSETS_3);

		stage.setTitle(error ? ERROR_TITLE : MESSAGE_TITLE);
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, uiStage.getMessageDialogHandler());
		stage.addEventHandler(WindowEvent.WINDOW_HIDDEN, uiStage.getMessageDialogHandler());
		stage.setMaxWidth(width);
		stage.setMinWidth(width);
		stage.setMaxHeight(heght);
		stage.setMinHeight(heght);
		stage.setScene(scene);
		stage.show();
		stage.centerOnScreen();
	}
}
