package com.ss.sniffer.ui;

import com.ss.sniffer.Start;
import com.ss.sniffer.ui.state.UIState;
import com.ss.sniffer.ui.state.impl.LoadUIState;

import javafx.application.Application;
import javafx.stage.Stage;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.Util;

/**
 * Стартовый класс для UI.
 * 
 * @author Ronn
 */
public class UIApplication extends Application {

	protected static final Logger LOGGER = LoggerManager.getLogger(Start.class);

	/** следующая стадия UI */
	private final UIState nextState = new LoadUIState();

	public UIApplication() {
		preapreRootClass();
	}

	protected void preapreRootClass() {
	}

	@Override
	public void start(final Stage stage) throws Exception {
		Util.safeExecute(() -> {
			nextState.start(stage);
		});
	}
}
