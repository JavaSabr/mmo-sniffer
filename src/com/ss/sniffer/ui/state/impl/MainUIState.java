package com.ss.sniffer.ui.state.impl;

import com.ss.sniffer.model.Session;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.model.impl.DefaultSession;
import com.ss.sniffer.net.Sniffer;
import com.ss.sniffer.ui.uistage.main.MainSessionTab;
import com.ss.sniffer.ui.uistage.main.MainTabPanel;
import com.ss.sniffer.ui.uistage.main.MenuBarPanel;
import com.ss.sniffer.model.impl.DefaultWorker;

import javafx.beans.value.ChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Главная рабочая стадия.
 * 
 * @author Ronn
 */
public class MainUIState extends AbstractUIState {

	private static final int STAGE_HEIGHT = 500;
	private static final int STAGE_WIDTH = 900;

	private final ChangeListener<Tab> changeSessionTab = (observable, oldValue, newValue) -> {
		if(newValue instanceof MainSessionTab) {
			getWorker().setCurrentSession(((MainSessionTab) newValue).getSession());
		}
	};

	/** работник */
	private Worker worker;

	/** сцена стадии */
	private Scene scene;

	/** рутовый лоаут */
	private VBox root;

	/** главная панель со вкладками */
	private MainTabPanel mainTabPanel;

	/** ядро сниера */
	@SuppressWarnings("unused")
	private Sniffer sniffer;

	public MainUIState() {
	}

	/**
	 * Добавление новой сессии.
	 * 
	 * @param id ид новой сессии.
	 * @return созданная сессия.
	 */
	public Session addSession(final int id) {

		final ObservableList<Tab> tabs = mainTabPanel.getTabs();

		final MainSessionTab tab = new MainSessionTab(worker);

		final Session session = new DefaultSession(id, tab);

		tab.prepare(this, session);

		tabs.add(tab);

		return session;
	}

	@Override
	public void disable() {
		root.setDisable(true);
	}

	@Override
	public void enable() {
		root.setDisable(false);
	}

	public MainTabPanel getMainTabPanel() {
		return mainTabPanel;
	}

	public Worker getWorker() {
		return worker;
	}

	@Override
	public void start(final Stage stage) {

		super.start(stage);

		worker = new DefaultWorker(this);

		root = new VBox();

		scene = new Scene(root, STAGE_WIDTH, STAGE_HEIGHT, Color.WHITESMOKE);

		final MenuBarPanel menuBar = new MenuBarPanel(this, worker);

		mainTabPanel = new MainTabPanel(this, worker);

		final SingleSelectionModel<Tab> selection = mainTabPanel.getSelectionModel();
		selection.selectedItemProperty().addListener(changeSessionTab);

		final ObservableList<Node> children = root.getChildren();
		children.add(menuBar);
		children.add(mainTabPanel);

		mainTabPanel.prefHeightProperty().bind(root.heightProperty());

		stage.setScene(scene);
		stage.show();
		stage.setResizable(true);
		stage.centerOnScreen();

		sniffer = new Sniffer(worker);
	}

	@Override
	public void stop() {
		super.stop();
	}
}
