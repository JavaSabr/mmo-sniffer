package com.ss.sniffer.ui.state.impl;

import com.ss.sniffer.Config;
import com.ss.sniffer.manager.ClassManager;
import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.Sniffer;
import com.ss.sniffer.net.util.NetUtil;
import com.ss.sniffer.ui.Constans;
import com.ss.sniffer.ui.UIUtils;
import com.ss.sniffer.ui.uistage.config.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.jnetpcap.PcapIf;
import rlib.util.array.Array;

import java.util.List;

/**
 * Стадия настройки активации снифера.
 * 
 * @author Ronn
 */
public class ConfigUIState extends AbstractUIState {

	private static final String BUTTON_NEXT_LABEL = "Продолжить";
	private static final String SNIFF_PORT_LABEL = "Порт";
	private static final String SNIFF_DEVICE_LABEL = "Прослушивоемое устройство";
	private static final String CRYPTOR_TYPE_LABEL = "Тип криптора";
	private static final String PACKET_MODEL_LABEL = "Пакетная модель";

	private static final int STATE_HEIGHT = 180;
	private static final int STATE_WIDTH = 560;
	private static final int SNIFF_PORT_COLUMN_WIDTH = 70;
	private static final int SNIFF_DEVICE_COLUMN_WIDTH = 430;
	private static final int CRYPTOR_TYPE_COLUMN_WIDTH = 250;
	private static final int PACKET_MODEL_COLUMN_WIDTH = 250;

	private final MainUIState nextStage;

	/** сцена стадии */
	private Scene scene;

	/** кнопка продолжения */
	private Button button;

	/** рут сцены */
	private VBox root;

	public ConfigUIState() {
		this.nextStage = new MainUIState();
	}

	@Override
	public void disable() {
		root.setDisable(true);
	}

	@Override
	public void enable() {
		root.setDisable(false);
	}

	/**
	 * @return кнопка продолжения.
	 */
	public Button getButton() {
		return button;
	}

	/**
	 * Подготовка крипторов.
	 */
	private void prepareCryptTypes(final ComboBox<CryptTypeItem> comboBox) {

		final ObservableList<CryptTypeItem> items = comboBox.getItems();

		final ClassManager manager = ClassManager.getInstance();

		final Array<CryptType> crypts = manager.getCryptTypes(this);

		for(final CryptType crypt : crypts) {
			items.add(new CryptTypeItem(crypt));
		}
	}

	/**
	 * Подготовка списка устройств.
	 */
	private void prepareDevices(final ComboBox<DeviceItem> comboBox) {

		final ObservableList<DeviceItem> items = comboBox.getItems();

		final List<PcapIf> devices = NetUtil.getAllDevice();

		for(final PcapIf device : devices) {
			items.add(new DeviceItem(device));
		}

		items.add(new DeviceItem(Sniffer.NULL_DEVICE));
	}

	/**
	 * Подготовка пакетных моделей.
	 */
	private void preparePacketModels(final ComboBox<PacketModelItem> comboBox) {

		final ObservableList<PacketModelItem> items = comboBox.getItems();

		final ClassManager manager = ClassManager.getInstance();

		final Array<PacketModel> models = manager.getPacketModels(this);

		for(final PacketModel model : models) {
			items.add(new PacketModelItem(model));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(final Stage stage) {
		super.start(stage);

		root = new VBox(2);
		root.setAlignment(Pos.CENTER);

		scene = new Scene(root, STATE_WIDTH, STATE_HEIGHT, Color.WHITESMOKE);

		/* первая строка с 2мя списками */
		HBox rowLayout = new HBox(2);
		rowLayout.setAlignment(Pos.CENTER);

		VBox columnLayout = new VBox(2);

		Label label = new Label(PACKET_MODEL_LABEL, new ImageView(UIUtils.loadImage("packet_model_16")));
		label.setTextAlignment(TextAlignment.CENTER);
		label.setMinWidth(PACKET_MODEL_COLUMN_WIDTH);
		label.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		label.setAlignment(Pos.CENTER);

		ComboBox<?> comboBox = new ComboBox<PacketModelItem>();
		comboBox.setMinWidth(PACKET_MODEL_COLUMN_WIDTH);
		comboBox.setMaxWidth(PACKET_MODEL_COLUMN_WIDTH);
		comboBox.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		comboBox.addEventHandler(ActionEvent.ACTION, new ChangePacketModelHandler(this));

		preparePacketModels((ComboBox<PacketModelItem>) comboBox);

		ObservableList<Node> children = columnLayout.getChildren();
		children.add(label);
		children.add(comboBox);

		VBox.setMargin(rowLayout, Constans.INSETS_2);
		VBox.setMargin(comboBox, Constans.INSETS_2);

		rowLayout.getChildren().add(columnLayout);

		columnLayout = new VBox(2);

		label = new Label(CRYPTOR_TYPE_LABEL, new ImageView(UIUtils.loadImage("crypt_16")));
		label.setAlignment(Pos.CENTER);
		label.setTextAlignment(TextAlignment.CENTER);
		label.setMinWidth(CRYPTOR_TYPE_COLUMN_WIDTH);
		label.setMinHeight(Config.MIN_ELEMENT_HEIGHT);

		comboBox = new ComboBox<CryptTypeItem>();
		comboBox.setMinWidth(CRYPTOR_TYPE_COLUMN_WIDTH);
		comboBox.setMaxWidth(CRYPTOR_TYPE_COLUMN_WIDTH);
		comboBox.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		comboBox.addEventHandler(ActionEvent.ACTION, new ChangeCryptTypeHandler(this));

		prepareCryptTypes((ComboBox<CryptTypeItem>) comboBox);

		children = columnLayout.getChildren();
		children.add(label);
		children.add(comboBox);

		rowLayout.getChildren().add(columnLayout);
		root.getChildren().add(rowLayout);

		VBox.setMargin(rowLayout, Constans.INSETS_2);
		VBox.setMargin(comboBox, Constans.INSETS_2);

		/* вторая строка с 1 списком и полем для указания порта */
		rowLayout = new HBox(2);
		rowLayout.setAlignment(Pos.CENTER);

		columnLayout = new VBox(2);

		label = new Label(SNIFF_DEVICE_LABEL, new ImageView(UIUtils.loadImage("network_device_16")));
		label.setAlignment(Pos.CENTER);
		label.setTextAlignment(TextAlignment.CENTER);
		label.setMinWidth(SNIFF_DEVICE_COLUMN_WIDTH);
		label.setMinHeight(Config.MIN_ELEMENT_HEIGHT);

		comboBox = new ComboBox<DeviceItem>();
		comboBox.setMinWidth(SNIFF_DEVICE_COLUMN_WIDTH);
		comboBox.setMaxWidth(SNIFF_DEVICE_COLUMN_WIDTH);
		comboBox.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		comboBox.addEventHandler(ActionEvent.ACTION, new ChangeDeviceHandler(this));

		prepareDevices((ComboBox<DeviceItem>) comboBox);

		children = columnLayout.getChildren();
		children.add(label);
		children.add(comboBox);

		VBox.setMargin(rowLayout, Constans.INSETS_2);
		VBox.setMargin(comboBox, Constans.INSETS_2);

		rowLayout.getChildren().add(columnLayout);

		columnLayout = new VBox(2);

		label = new Label(SNIFF_PORT_LABEL, new ImageView(UIUtils.loadImage("network_port_16")));
		label.setAlignment(Pos.CENTER);
		label.setTextAlignment(TextAlignment.CENTER);
		label.setMinWidth(SNIFF_PORT_COLUMN_WIDTH);
		label.setMinHeight(Config.MIN_ELEMENT_HEIGHT);

		final TextField textField = new TextField();
		textField.setMinWidth(SNIFF_PORT_COLUMN_WIDTH);
		textField.setMaxWidth(SNIFF_PORT_COLUMN_WIDTH);
		textField.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		textField.addEventHandler(KeyEvent.KEY_PRESSED, new PortValidateHandler(this, textField));

		children = columnLayout.getChildren();
		children.add(label);
		children.add(textField);

		VBox.setMargin(rowLayout, Constans.INSETS_2);
		VBox.setMargin(textField, Constans.INSETS_2);

		rowLayout.getChildren().add(columnLayout);
		root.getChildren().add(rowLayout);

		button = new Button(BUTTON_NEXT_LABEL, new ImageView(UIUtils.loadImage("next_16")));
		button.setMinHeight(Config.MIN_ELEMENT_HEIGHT);
		button.setDisable(true);
		button.addEventHandler(ActionEvent.ACTION, event -> gotoNext(nextStage));

		root.getChildren().add(button);

		stage.show();
		stage.setScene(scene);
		stage.setResizable(false);
		stage.centerOnScreen();
	}

	@Override
	public void stop() {
		super.stop();

		scene = null;
		button = null;
	}

	/**
	 * Обновление активности кнопки.
	 */
	public void update() {

		final Button button = getButton();

		boolean disable = true;

		try {
			disable = Config.CRYPT_TYPE == null;
			disable = disable || Config.PACKET_MODEL == null;
			disable = disable || !Config.CRYPT_TYPE.validate(Config.PACKET_MODEL);
			disable = disable || !Config.PACKET_MODEL.validate(Config.CRYPT_TYPE);
			disable = disable || Config.NETWORK_DEVICE == null;
			disable = disable || Config.NETWORK_DEVICE != Sniffer.NULL_DEVICE && Config.NETWORK_PORT < 1;
		} finally {
			button.setDisable(disable);
		}
	}
}
