package com.ss.sniffer;

import javafx.scene.text.Font;

import org.jnetpcap.PcapIf;
import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;

/**
 * Конфиг работы снифера.
 * 
 * @author Ronn
 */
public final class Config {

	public static final Font DEFAULT_FONT = Font.getDefault();
	public static final Font MESSAGE_FONT = Font.getDefault();

	public static final String TITLE = "MMO Sniffer by Ronn";

	public static final int MIN_ELEMENT_HEIGHT = 23;

	public static Class<?> ROOT_CLASS;

	public static PcapIf NETWORK_DEVICE;

	public static CryptType CRYPT_TYPE;

	public static PacketModel PACKET_MODEL;

	public static int NETWORK_PORT;

	private Config() {
		throw new RuntimeException();
	}
}
