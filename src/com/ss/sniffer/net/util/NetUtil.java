package com.ss.sniffer.net.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import com.ss.sniffer.net.PacketBuilder;
import com.ss.sniffer.net.impl.PacketReader;
import com.ss.sniffer.net.packet.PacketType;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import com.ss.sniffer.net.packet.Packet;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.util.array.Array;

/**
 * Набор утильных методов связанных с сетевой логикой.
 * 
 * @author Ronn
 */
public final class NetUtil {

	private static final Logger LOGGER = LoggerManager.getLogger(NetUtil.class);

	public static final String SESSION_DUMP_FORMAT = ".mssd";

	/**
	 * Получение всех доступных сетевых устройств для прослушки.
	 * 
	 * @return список доступных устройств.
	 */
	public static final List<PcapIf> getAllDevice() {

		final StringBuilder error = new StringBuilder();

		final List<PcapIf> devices = new ArrayList<PcapIf>();

		final int result = Pcap.findAllDevs(devices, error);

		if(result == Pcap.NOT_OK) {
			throw new RuntimeException(error.toString());
		}

		return devices;
	}

	/**
	 * Загрука сессии из файла.
	 * 
	 * @param file файл с сохраненной сессией.
	 * @param container контейнер для пакетов.
	 */
	public static void load(final File file, final Array<Packet> container) {

		final PacketBuilder builder = PacketReader.getBuilder();

		ByteBuffer buffer = ByteBuffer.allocate(30).order(ByteOrder.BIG_ENDIAN);

		final PacketType[] types = PacketType.values();

		try(FileInputStream fileInputStream = new FileInputStream(file)) {

			final FileChannel channel = fileInputStream.getChannel();

			buffer.clear();
			buffer.limit(4);

			channel.read(buffer);

			buffer.flip();

			final int size = buffer.getInt();

			for(int i = 0; i < size; i++) {

				buffer.clear();
				buffer.limit(25);

				channel.read(buffer);

				buffer.flip();

				final long timestamp = buffer.getLong();

				final int destination = buffer.getInt();
				final int source = buffer.getInt();
				final int sessionId = buffer.getInt();

				final PacketType type = types[buffer.get()];

				final int length = buffer.getInt();

				if(buffer.capacity() < length) {
					buffer = ByteBuffer.allocate(length).order(ByteOrder.BIG_ENDIAN);
				}

				buffer.clear();
				buffer.limit(length);

				channel.read(buffer);

				final byte[] data = new byte[length];

				buffer.flip();
				buffer.get(data);

				final Packet packet = builder.build(data, source, destination, sessionId, timestamp, type == PacketType.SERVER_PACKET);
				container.add(packet);
			}
		} catch(final Exception e) {
			LOGGER.error(e);
		}
	}

	/**
	 * Сохранение списка пакетов в фаил.
	 * 
	 * @param file фаил, для сохранения пакетов.
	 * @param packets список сохраняемых пакетов.
	 */
	public static void save(final File file, final Array<Packet> packets) {

		ByteBuffer buffer = ByteBuffer.allocate(30).order(ByteOrder.BIG_ENDIAN);

		try(FileOutputStream fileOutputStream = new FileOutputStream(file)) {

			final FileChannel channel = fileOutputStream.getChannel();

			buffer.clear();
			buffer.putInt(packets.size());
			buffer.flip();

			channel.write(buffer);

			for(final Packet packet : packets) {

				final byte[] data = packet.getData();

				buffer.clear();
				buffer.putLong(packet.getTimestamp());
				buffer.putInt(packet.getDestination());
				buffer.putInt(packet.getSource());
				buffer.putInt(packet.getSessionId());
				buffer.put((byte) packet.getType().ordinal());
				buffer.putInt(data.length);
				buffer.flip();

				channel.write(buffer);

				if(buffer.capacity() < data.length) {
					buffer = ByteBuffer.allocate(data.length).order(ByteOrder.BIG_ENDIAN);
				}

				buffer.clear();
				buffer.put(data);
				buffer.flip();

				channel.write(buffer);
			}
		} catch(final IOException e) {
			LOGGER.error(e);
		}
	}

	public static String toHEX(final byte[] array) {

		final StringBuilder builder = new StringBuilder(array.length * 2 + array.length);

		for(final byte val : array) {

			final String hex = Integer.toHexString(val & 0xFF).toUpperCase();

			if(hex.length() < 2) {
				builder.append('0');
			}

			builder.append(hex);
			builder.append(' ');
		}

		return builder.toString();
	}

	private NetUtil() {
		throw new RuntimeException();
	}
}
