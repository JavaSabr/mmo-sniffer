package com.ss.sniffer.net.packet;

/**
 * Интерфейс для реализации пакета.
 * 
 * @author Ronn
 */
public interface Packet {

	/**
	 * @return данные пакета.
	 */
	public byte[] getData();

	/**
	 * @return целевое назначение пакета.
	 */
	public int getDestination();

	/**
	 * @return ид сессии пакета.
	 */
	public int getSessionId();

	/**
	 * @return источник пакета.
	 */
	public int getSource();

	/**
	 * @return время получения пакета.
	 */
	public long getTimestamp();

	/**
	 * @return тип пакета.
	 */
	public PacketType getType();

	/**
	 * @param data данные пакета.
	 */
	public void setData(byte[] data);

	/**
	 * @param sessionId ид сессии пакета.
	 */
	public void setSessionId(int sessionId);

	/**
	 * @param timestamp время получения пакета.
	 */
	public void setTimestamp(long timestamp);
}
