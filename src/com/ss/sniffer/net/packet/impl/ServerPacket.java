package com.ss.sniffer.net.packet.impl;

import com.ss.sniffer.net.packet.PacketType;

/**
 * Модель серверного пакета.
 * 
 * @author Ronn
 */
public final class ServerPacket extends AbstractPacket {

	public ServerPacket(final int source, final int destination) {
		super(source, destination);
	}

	@Override
	public PacketType getType() {
		return PacketType.SERVER_PACKET;
	}
}
