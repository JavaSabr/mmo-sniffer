package com.ss.sniffer.net.packet.impl;

import com.ss.sniffer.net.packet.PacketType;

/**
 * Модель клиентского пакета.
 * 
 * @author Ronn
 */
public class ClientPacket extends AbstractPacket {

	public ClientPacket(final int source, final int destination) {
		super(source, destination);
	}

	@Override
	public PacketType getType() {
		return PacketType.CLIENT_PACKET;
	}
}
