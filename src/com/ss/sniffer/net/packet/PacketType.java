package com.ss.sniffer.net.packet;

/**
 * Перечисление типов пакетов.
 * 
 * @author Ronn
 */
public enum PacketType {
	SERVER_PACKET,
	CLIENT_PACKET,
}
