package com.ss.sniffer.net;

import java.util.List;

import com.ss.sniffer.Config;
import org.jnetpcap.Pcap;
import org.jnetpcap.PcapAddr;
import org.jnetpcap.PcapBpfProgram;
import org.jnetpcap.PcapIf;
import com.ss.sniffer.manager.ExecutorManager;
import com.ss.sniffer.model.Worker;
import com.ss.sniffer.net.impl.SnifferPacketHandler;

import rlib.util.NumberUtils;
import rlib.util.array.ArrayFactory;
import rlib.util.array.IntegerArray;

/**
 * Реализация процесса снифа пакетов.
 * 
 * @author Ronn
 */
public class Sniffer {

	public static final PcapIf NULL_DEVICE = new PcapIf();

	/** массив клиентских адрессов */
	private final IntegerArray clientAddresses;

	/** работник */
	private final Worker worker;

	public Sniffer(final Worker worker) {

		this.clientAddresses = ArrayFactory.newIntegerArray();
		this.worker = worker;

		final PcapIf device = Config.NETWORK_DEVICE;

		if(device == NULL_DEVICE) {
			return;
		}

		final Pcap pcap = openDevice(device);

		worker.sendMessage("Открыто устройство " + device.getName());
		prepareFilter(pcap);
		worker.sendMessage("Подготовлен фильтр на порт " + Config.NETWORK_PORT);
		prepareClientAddresses(device);
		worker.sendMessage("Определены клиентские адреса " + getClientAddresses());

		final SnifferPacketHandler packetHandler = new SnifferPacketHandler(this) {

			@Override
			protected Void call() throws Exception {

				final Worker worker = getWorker();
				worker.sendMessage("Запуск прослушивания пакетов.");

				pcap.loop(Integer.MAX_VALUE, this, worker);

				return null;
			}
		};

		final ExecutorManager manager = ExecutorManager.getInstance();
		manager.submit(packetHandler);
	}

	/**
	 * @return список адресов клиента.
	 */
	public IntegerArray getClientAddresses() {
		return clientAddresses;
	}

	/**
	 * @return работник.
	 */
	private Worker getWorker() {
		return worker;
	}

	/**
	 * Определение, является ли указанный адресс клиентским.
	 * 
	 * @param adress проверяемый адресс.
	 * @return является ли адресс клиентским.
	 */
	public final boolean isClientAdress(final int address) {
		return clientAddresses.contains(address);
	}

	/**
	 * Открытие нужного устройства.
	 */
	private Pcap openDevice(final PcapIf device) {

		final StringBuilder error = new StringBuilder();

		final int snaplen = 64 * 1024;
		final int flags = Pcap.MODE_PROMISCUOUS;
		final int timeout = 10 * 1000;

		final Pcap pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout, error);

		if(pcap == null) {
			throw new IllegalArgumentException(error.toString());
		}

		return pcap;
	}

	/**
	 * Определяем текущие клиентские адреса.
	 */
	private void prepareClientAddresses(final PcapIf device) {

		final IntegerArray clientAddresses = getClientAddresses();

		final List<PcapAddr> addresses = device.getAddresses();

		for(final PcapAddr address : addresses) {

			final byte[] data = address.getAddr().getData();

			if(data.length > 4) {
				continue;
			}

			clientAddresses.add(NumberUtils.bytesToInt(data, 0, false));
		}
	}

	/**
	 * Подготовка фильтра пакетов.
	 */
	private void prepareFilter(final Pcap pcap) {

		final PcapBpfProgram filter = new PcapBpfProgram();

		final String expression = "port " + Config.NETWORK_PORT;

		final int optimize = 0;
		final int netmask = 0;

		final int result = pcap.compile(filter, expression, optimize, netmask);

		if(result != Pcap.OK) {
			throw new IllegalArgumentException("Filter error: " + pcap.getErr());
		}

		pcap.setFilter(filter);
	}
}
