package com.ss.sniffer.net.impl;

import com.ss.sniffer.net.PacketBuilder;
import com.ss.sniffer.net.Sniffer;
import com.ss.sniffer.net.packet.impl.ClientPacket;
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;
import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.net.packet.impl.ServerPacket;

import rlib.util.ArrayUtils;

/**
 * Модель читальщика пакетов.
 * 
 * @author Ronn
 */
public class PacketReader implements PacketBuilder {

	private static final PacketBuilder builder = new PacketReader();

	public static PacketBuilder getBuilder() {
		return builder;
	}

	/** заголовок ТСП пакета */
	private final Tcp tcp;
	/** заголовок ИП пакета */
	private final Ip4 ip;

	/** данные пакета */
	private final Payload load;

	protected PacketReader() {
		this.ip = new Ip4();
		this.tcp = new Tcp();
		this.load = new Payload();
	}

	@Override
	public Packet build(final byte[] data, final int sourceIp, final int destinationIp, final int sessionId, final long timestamp, final boolean server) {

		final Packet packet = server ? new ServerPacket(sourceIp, destinationIp) : new ClientPacket(sourceIp, destinationIp);
		packet.setData(data);
		packet.setSessionId(sessionId);
		packet.setTimestamp(timestamp);

		return packet;
	}

	/**
	 * Проверка на пригодность пакета.
	 * 
	 * @param packet исходный пакет.
	 * @return пригоден ли для обработки.
	 */
	protected boolean check(final PcapPacket packet) {
		return packet.hasHeader(tcp) && packet.hasHeader(ip) && packet.hasHeader(load);
	}

	/**
	 * Парсер выснифанного пакета.
	 */
	protected Packet read(final Sniffer sniffer, final PcapPacket source) {

		source.getHeader(ip);

		final int sourceIp = ip.sourceToInt();
		final int destionationIp = ip.destinationToInt();

		source.getHeader(tcp);
		source.getHeader(load);

		final byte[] data = ArrayUtils.copyOf(load.data(), 0);

		final int sessionId = tcp.source() * tcp.destination();

		return build(data, sourceIp, destionationIp, sessionId, source.getCaptureHeader().timestampInMillis(), !sniffer.isClientAdress(sourceIp));
	}
}
