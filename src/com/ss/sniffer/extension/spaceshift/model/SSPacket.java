package com.ss.sniffer.extension.spaceshift.model;

import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.ui.model.packet.AbstractUIPacket;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Реализация UI пакета для Space Shift.
 * 
 * @author Ronn
 */
public class SSPacket extends AbstractUIPacket {

	public SSPacket(final Packet packet) {
		super(packet);
	}

	@Override
	protected void prepare() {

		final Packet packet = getPacket();

		final byte[] data = packet.getData();

		final ByteBuffer buffer = ByteBuffer.allocate(data.length).order(ByteOrder.LITTLE_ENDIAN);
		buffer.clear();
		buffer.put(data);
		buffer.flip();

		final int size = buffer.getShort() & 0xFFFF;
		final int opcode = buffer.getShort() & 0xFFFF;

		setSize(size);
		setOpcode(opcode);

		buffer.clear();
		buffer.put(data, 4, data.length - 4);

		setBuffer(buffer);
	}
}
