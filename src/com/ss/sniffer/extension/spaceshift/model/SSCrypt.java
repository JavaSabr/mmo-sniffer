package com.ss.sniffer.extension.spaceshift.model;

import com.ss.sniffer.net.Crypt;
import rlib.util.SymmetryCrypt;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Реализация криптора Space Shift.
 * 
 * @author Ronn
 */
public class SSCrypt implements Crypt {

	private SymmetryCrypt crypt;

	public SSCrypt() {
		try {
			this.crypt = new SymmetryCrypt(":qYx5GE?");
		} catch(InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void decrypt(final byte[] data, final int offset, final int length, final boolean server) {
		try {
			crypt.decrypt(data, offset + 2, length - 2, data);
		} catch(ShortBufferException | IllegalBlockSizeException | BadPaddingException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isReady() {
		return true;
	}
}
