package com.ss.sniffer.extension.spaceshift.model;

import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.Crypt;

/**
 * Реализация криптора для Space Shift;
 * 
 * @author Ronn
 */
public class SSCryptType implements CryptType {

	private static final String NAME = "Space Shift Crypt";

	@Override
	public Crypt create() {
		return new SSCrypt();
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public boolean validate(final PacketModel model) {
		return model instanceof SSPacketModel;
	}
}
