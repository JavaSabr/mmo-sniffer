package com.ss.sniffer.extension.l2.model;

import com.ss.sniffer.model.CryptType;
import com.ss.sniffer.model.PacketModel;
import com.ss.sniffer.net.Crypt;

/**
 * @author Ronn
 */
public class L2CryptType implements CryptType {

	@Override
	public Crypt create() {
		return new CryptImpl();
	}

	@Override
	public String getName() {
		return "Lineage II Crypt";
	}

	@Override
	public boolean validate(final PacketModel model) {
		return model instanceof L2PacketModel;
	}
}
