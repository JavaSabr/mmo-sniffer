package com.ss.sniffer.extension.tera.model.crypt;

import rlib.util.ArrayUtils;

/**
 * Криптор/декриптор пакетов Tera-Online.
 * 
 * @author Ronn
 */
public final class TeraCrypt implements com.ss.sniffer.net.Crypt {

	/** декриптор */
	private Crypt decrypt;

	/** криптор */
	private Crypt encrypt;

	/** временный массивы байтов */
	private final byte[][] temps;

	/** статус криптора */
	private CryptorState state;

	public TeraCrypt() {
		decrypt = new Crypt();
		encrypt = new Crypt();

		temps = new byte[4][];

		state = CryptorState.WAIT_FIRST_CLIENT_KEY;
	}

	/**
	 * Очистка криптора для переиспользования.
	 */
	public void clear() {
		state = CryptorState.WAIT_FIRST_CLIENT_KEY;

		decrypt = new Crypt();
		encrypt = new Crypt();
	}

	@Override
	@SuppressWarnings("incomplete-switch")
	public void decrypt(final byte[] data, final int offset, final int length, final boolean server) {
		if(server) {
			switch(state) {
				case READY_TO_WORK:
					encrypt.applyCryptor(data, length);
					break;
				case WAIT_FIRST_SERVER_KEY: {
					if(length == 128) {
						temps[1] = ArrayUtils.copyOf(data, 128);
						state = CryptorState.WAIT_SECOND_CLIENT_KEY;
					}

					break;
				}
				case WAIT_SECOND_SERCER_KEY: {
					if(length == 128) {
						temps[3] = ArrayUtils.copyOf(data, 128);

						final byte[] firstTemp = new byte[128];
						final byte[] secondTemp = new byte[128];
						final byte[] cryptKey = new byte[128];

						Crypt.shiftKey(temps[1], firstTemp, 31, true);
						Crypt.xorKey(firstTemp, temps[0], secondTemp);
						Crypt.shiftKey(temps[2], firstTemp, 17, false);
						Crypt.xorKey(firstTemp, secondTemp, cryptKey);

						decrypt.generateKey(cryptKey);

						Crypt.shiftKey(temps[3], firstTemp, 79, true);

						decrypt.applyCryptor(firstTemp, 128);

						encrypt.generateKey(firstTemp);

						ArrayUtils.clear(temps);

						state = CryptorState.READY_TO_WORK;
					}
				}
			}
		} else {
			switch(state) {
				case READY_TO_WORK:
					decrypt.applyCryptor(data, length);
					break;
				case WAIT_FIRST_CLIENT_KEY: {
					if(length == 128) {
						temps[0] = ArrayUtils.copyOf(data, 128);
						state = CryptorState.WAIT_FIRST_SERVER_KEY;
					}

					break;
				}
				case WAIT_SECOND_CLIENT_KEY: {
					if(length == 128) {
						temps[2] = ArrayUtils.copyOf(data, 128);
						state = CryptorState.WAIT_SECOND_SERCER_KEY;
					}
				}
			}
		}
	}

	/**
	 * @return статус криптора.
	 */
	public CryptorState getState() {
		return state;
	}

	@Override
	public boolean isReady() {
		return state == CryptorState.READY_TO_WORK;
	}

	/**
	 * @param state статус криптора.
	 */
	public void setState(final CryptorState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "TeraCrypt decrypt = " + decrypt + ", encrypt = " + encrypt;
	}
}