package com.ss.sniffer.extension.tera.model;

import com.ss.sniffer.net.packet.Packet;
import com.ss.sniffer.ui.model.packet.AbstractUIPacket;
import rlib.logging.Logger;
import rlib.logging.LoggerManager;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


/**
 * Реализация UI пакета для Tera Online.
 * 
 * @author Ronn
 */
public class TeraPacket extends AbstractUIPacket {

	private static final Logger LOGGER = LoggerManager.getLogger(TeraPacket.class);

	public TeraPacket(final Packet packet) {
		super(packet);
	}

	@Override
	public String getName() {
		return "Packet";
	}

	@Override
	protected void prepare() {
		try {

			final Packet packet = getPacket();

			final byte[] data = packet.getData();

			final ByteBuffer buffer = ByteBuffer.allocate(data.length).order(ByteOrder.LITTLE_ENDIAN);
			buffer.clear();
			buffer.put(data);
			buffer.flip();

			final int size = buffer.getShort() & 0xFFFF;
			final int opcode = buffer.getShort() & 0xFFFF;

			setSize(size);
			setOpcode(opcode);

			buffer.clear();
			buffer.put(data, 4, data.length - 4);

			setBuffer(buffer);
		} catch(final Exception e) {
			LOGGER.warning(e);
		}
	}
}
